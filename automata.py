import tkinter as tk
import random
import copy
# import matplotlib.pyplot as plt

class CellularAutomaton:
    def __init__(self, rows, cols, cell_size=10):
        self.rows = rows
        self.cols = cols
        self.cell_size = cell_size
        self.grid = [[0 for _ in range(cols)] for _ in range(rows)]
        self.generation = 0
        self.max_generations = 250
        self.scores = []  # To store the score at each generation
        self.initialize_grid()  # Initialize the grid with random values
        self.create_gui()  # Setup the GUI

    def initialize_grid(self):
        # Randomly initialize half of the cells to white (0) and the other half to black (1)
        cells = [0] * (self.rows * self.cols // 2) + [1] * (self.rows * self.cols // 2)
        random.shuffle(cells)
        for i in range(self.rows):
            for j in range(self.cols):
                self.grid[i][j] = cells.pop()

    def create_gui(self):
        # Setup the GUI components - including the generation and score labels
        self.root = tk.Tk()
        self.root.title("Cellular Automaton - Zebra Pattern")
        self.canvas = tk.Canvas(self.root, width=self.cols * self.cell_size, height=self.rows * self.cell_size)
        self.canvas.pack()
        self.generation_label = tk.Label(self.root, text=f"Generation: {self.generation}")
        self.generation_label.pack()
        self.score_label = tk.Label(self.root, text=f"Score: {self.calculate_zebra_score():.4f}")
        self.score_label.pack()
        self.update_grid()

    def update_grid(self):
        # Update the grid for each generation 
        if self.generation < self.max_generations:
            self.apply_zebra_pattern_rule() # Apply the cellular automaton rule
            self.draw_grid() # Draw the current grid
            self.generation += 1
            score = self.calculate_zebra_score() # Calculate the zebra score
            self.scores.append(score) # Store the score for plotting the graph
            self.generation_label.config(text=f"Generation: {self.generation}")
            self.score_label.config(text=f"Score: {score:.4f}")
            
            if self.generation <= 1:
                self.root.after(1000, self.update_grid) # Slow update for the first generation
            else:
                self.root.after(100, self.update_grid)  # Update the grid every 100 ms (10 updates per second)
        else:
            # self.plot_scores() # Plot the scores after all generations are completed
            self.root.quit()  # Stop the GUI after 250 generations

    def apply_zebra_pattern_rule(self):
        # Apply the rules to transform the grid towards a zebra pattern 
        new_grid = copy.deepcopy(self.grid)
        for row in range(self.rows):
            for col in range(self.cols):
                #  If the current cell and its surrounding cells form a 6 cells match, randomly flip the cell
                if col < 79 and row >0 and row <79 and new_grid[row+1][col+1]== new_grid[row-1][col+1]== new_grid[row][col+1] == new_grid[row][col] == new_grid[row-1][col] == new_grid[row+1][col]:
                    new_grid[row][col] = random.choice([0, 1])
                # If the current cell and its surrounding cells form a 6 cells match but in another direction, randomly flip the cell 
                elif col > 0 and row < 79 and row >0 and new_grid[row-1][col-1] == new_grid[row+1][col-1] == new_grid[row][col-1] == new_grid[row][col] == new_grid[row-1][col] == new_grid[row+1][col]:
                    new_grid[row][col] = random.choice([0, 1])
                    
                # If the cells directly above and below the current cell match, set the current cell to this value          
                elif row < 79 and row > 0 and new_grid[row+1][col] == new_grid[row-1][col]:
                    new_grid[row][col] = new_grid[row+1][col]
                    
                # If the cells directly to the left and right of the current cell match, set the current cell to the opposite value                    
                elif col < 79 and col > 0 and new_grid[row][col+1] == new_grid[row][col-1]:
                    if new_grid[row][col+1] == 0: 
                        new_grid[row][col] = 1
                    else:
                        new_grid[row][col] = 0   
                
                

                               
                # If the 3 cells from the column to the one side of the cell match, set the current cell to the opposite value
                elif col < 79 and row > 0 and row < 79 and new_grid[row+1][col+1]== new_grid[row-1][col+1]== new_grid[row][col+1]:
                    if new_grid[row-1][col+1] == 0: 
                        new_grid[row][col] = 1
                    else:
                        new_grid[row][col] = 0
                        
                # If the 3 cells from the column to the other side of the cell match, set the current cell to the opposite value          
                elif col > 0 and row < 79 and row > 0 and new_grid[row-1][col-1] == new_grid[row+1][col-1] == new_grid[row][col-1]:
                    if new_grid[row+1][col-1] == 0: 
                        new_grid[row][col] = 1
                    else:
                        new_grid[row][col] = 0 
                        
                # If the diagonal cells form a cross pattern and match, set the current cell to the opposite value                          
                elif col > 0 and col < 79 and row < 79 and row > 0 and new_grid[row+1][col+1] == new_grid[row-1][col-1]== new_grid[row+1][col-1]== new_grid[row-1][col+1]:
                    if new_grid[row+1][col+1] == 0: 
                        new_grid[row][col] = 1
                    else:
                        new_grid[row][col] = 0   
                           
                # If the cell above and to the left is different from the current cell, copy the value from above                                
                elif row > 0 and col>0 and new_grid[row-1][col] != new_grid[row][col-1]:
                    new_grid[row][col] = new_grid[row-1][col]
                # If the cell below and to the right is different from the current cell, copy the value from above
                elif row < 79 and col<79 and new_grid[row+1][col] != new_grid[row][col+1]:
                    new_grid[row][col] = new_grid[row-1][col]
                # If the cell above and to the right is different from the current cell, copy the value from above
                elif row > 0 and col<79 and new_grid[row-1][col] != new_grid[row][col+1]:
                    new_grid[row][col] = new_grid[row-1][col]
                # If the cell below and to the left is different from the current cell, copy the value from above
                elif row < 79 and col>0 and new_grid[row+1][col] != new_grid[row][col-1]:
                    new_grid[row][col] = new_grid[row-1][col]
                    
                # If the cells directly above and below are different, randomly choose a new value                    
                elif row < 79 and row > 0 and new_grid[row+1][col] != new_grid[row-1][col]:
                    new_grid[row][col] = random.choice([0, 1])
                # If the cells directly to the left and right are different, randomly choose a new value
                elif col > 0 and col < 79 and new_grid[row][col-1] != new_grid[row][col+1]:
                    new_grid[row][col] = random.choice([0, 1])

        self.grid = new_grid

    def draw_grid(self):
        # Draw the current state of the grid on the canvas
        self.canvas.delete("all")
        for row in range(self.rows):
            for col in range(self.cols):
                color = "white" if self.grid[row][col] == 0 else "black"
                x1 = col * self.cell_size
                y1 = row * self.cell_size
                x2 = x1 + self.cell_size
                y2 = y1 + self.cell_size
                self.canvas.create_rectangle(x1, y1, x2, y2, fill=color, outline="gray")
                
    def calculate_zebra_score(self):
        # Calculate how close the current grid is to the zebra pattern 
        discrepancies_1 = 0
        discrepancies_2 = 0

        for row in range(self.rows):
            for col in range(self.cols):
                expected_state_1 = col % 2  # Expected state if starting with white
                expected_state_2 = (col + 1) % 2  # Expected state if starting with black
                
                if self.grid[row][col] != expected_state_1:
                    discrepancies_1 += 1
                if self.grid[row][col] != expected_state_2:
                    discrepancies_2 += 1

        total_cells = self.rows * self.cols
        min_discrepancies = min(discrepancies_1, discrepancies_2)
        return 1 - (min_discrepancies / total_cells) # Score between 0 and 1 
    
    # def plot_scores(self):
    #     # Plot the scores of each generation
    #     plt.figure(figsize=(10, 6))
    #     plt.plot(range(len(self.scores)), self.scores, marker='o')
    #     plt.xlabel('Generation')
    #     plt.ylabel('Zebra Score')
    #     plt.title('Zebra Score Progression')
    #     plt.grid(True)
    #     plt.xticks(range(0, self.max_generations + 1, 10))
    #     plt.yticks([i / 10 for i in range(11)])
    #     plt.show()


    def run(self):
        # Start the GUI loop
        self.root.mainloop()

if __name__ == "__main__":
    # Initialize and run the cellular automaton
    automaton = CellularAutomaton(rows=80, cols=80, cell_size=10)
    automaton.run()
